(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_empresa_empresa_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/empresa/empresa.component */ "./src/app/components/empresa/empresa.component.ts");
/* harmony import */ var _components_galeria_galeria_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/galeria/galeria.component */ "./src/app/components/galeria/galeria.component.ts");
/* harmony import */ var _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/contacto/contacto.component */ "./src/app/components/contacto/contacto.component.ts");
/* harmony import */ var _components_commingsoon_commingsoon_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/commingsoon/commingsoon.component */ "./src/app/components/commingsoon/commingsoon.component.ts");
/* harmony import */ var _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/portafolios/portafolios.component */ "./src/app/components/portafolios/portafolios.component.ts");
/* harmony import */ var _components_publicaciones_publicaciones_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/publicaciones/publicaciones.component */ "./src/app/components/publicaciones/publicaciones.component.ts");








var routes = [
    { path: 'home', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
    { path: 'empresa', component: _components_empresa_empresa_component__WEBPACK_IMPORTED_MODULE_2__["EmpresaComponent"] },
    { path: 'galeria', component: _components_galeria_galeria_component__WEBPACK_IMPORTED_MODULE_3__["GaleriaComponent"] },
    { path: 'contacto', component: _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_4__["ContactoComponent"] },
    { path: 'estaciones', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'estanterias', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'sillas', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'mesas', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'counters', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'archivadores', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'escritorios', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    // { path: 'credenzas', component: PortafoliosComponent },
    { path: 'closets', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'cocinas', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'gavetas', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'divisiones', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'cafeinternet', component: _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_6__["PortafoliosComponent"] },
    { path: 'commingsoon', component: _components_commingsoon_commingsoon_component__WEBPACK_IMPORTED_MODULE_5__["CommingsoonComponent"] },
    { path: 'publicaciones', component: _components_publicaciones_publicaciones_component__WEBPACK_IMPORTED_MODULE_7__["PublicacionesComponent"] },
    { path: '**', pathMatch: 'full', redirectTo: 'commingsoon' }
];
var AppRoutingModule = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes);


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<router-outlet></router-outlet>\r\n<app-footer></app-footer>\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'mobiliariosfamarsa';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _components_shared_header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/shared/header/header.component */ "./src/app/components/shared/header/header.component.ts");
/* harmony import */ var _components_shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/shared/footer/footer.component */ "./src/app/components/shared/footer/footer.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _components_empresa_empresa_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/empresa/empresa.component */ "./src/app/components/empresa/empresa.component.ts");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");
/* harmony import */ var _components_home_map_map_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/home/map/map.component */ "./src/app/components/home/map/map.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _components_galeria_galeria_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/galeria/galeria.component */ "./src/app/components/galeria/galeria.component.ts");
/* harmony import */ var _ngx_gallery_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ngx-gallery/core */ "./node_modules/@ngx-gallery/core/fesm5/ngx-gallery-core.js");
/* harmony import */ var _ngx_gallery_lightbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ngx-gallery/lightbox */ "./node_modules/@ngx-gallery/lightbox/fesm5/ngx-gallery-lightbox.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/contacto/contacto.component */ "./src/app/components/contacto/contacto.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_commingsoon_commingsoon_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/commingsoon/commingsoon.component */ "./src/app/components/commingsoon/commingsoon.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_shared_chats_chats_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/shared/chats/chats.component */ "./src/app/components/shared/chats/chats.component.ts");
/* harmony import */ var _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/portafolios/portafolios.component */ "./src/app/components/portafolios/portafolios.component.ts");
/* harmony import */ var _components_publicaciones_publicaciones_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/publicaciones/publicaciones.component */ "./src/app/components/publicaciones/publicaciones.component.ts");




// NgBootstrap




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_shared_header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"],
                _components_shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_6__["FooterComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
                _components_empresa_empresa_component__WEBPACK_IMPORTED_MODULE_9__["EmpresaComponent"],
                _components_home_map_map_component__WEBPACK_IMPORTED_MODULE_11__["MapComponent"],
                _components_galeria_galeria_component__WEBPACK_IMPORTED_MODULE_13__["GaleriaComponent"],
                _components_contacto_contacto_component__WEBPACK_IMPORTED_MODULE_17__["ContactoComponent"],
                _components_commingsoon_commingsoon_component__WEBPACK_IMPORTED_MODULE_19__["CommingsoonComponent"],
                _components_shared_chats_chats_component__WEBPACK_IMPORTED_MODULE_21__["ChatsComponent"],
                _components_portafolios_portafolios_component__WEBPACK_IMPORTED_MODULE_22__["PortafoliosComponent"],
                _components_publicaciones_publicaciones_component__WEBPACK_IMPORTED_MODULE_23__["PublicacionesComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["BrowserAnimationsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_12__["AgmCoreModule"].forRoot({ apiKey: 'AIzaSyA5mjCwx1TRLuBAjwQw84WE6h5ErSe7Uj8' }),
                _ngx_gallery_core__WEBPACK_IMPORTED_MODULE_14__["GalleryModule"],
                _ngx_gallery_lightbox__WEBPACK_IMPORTED_MODULE_15__["LightboxModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HttpClientModule"]
            ],
            providers: [
                _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_10__["MfconfigService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/commingsoon/commingsoon.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/commingsoon/commingsoon.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbWluZ3Nvb24vY29tbWluZ3Nvb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/commingsoon/commingsoon.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/commingsoon/commingsoon.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-6 offset-3 text-center\">\r\n<!--            <img src=\"{{image}}\" class=\"w-50\" alt=\"\">-->\r\n        </div>\r\n        <div class=\"col-md-12 text-center my-5\">\r\n            <h2>{{ textCommingSoon.title }}</h2>\r\n            <p>{{ textCommingSoon.textParrafo}}</p>\r\n        </div>\r\n        <div class=\"col-md-12 text-center py-5\">\r\n            <ul class=\"list-unstyled display-4\" style=\"display: inline-flex; \">\r\n                <li class=\"px-3\">\r\n                    <a href=\"{{textCommingSoon.facebook}}\">\r\n                        <i class=\"fab fa-facebook-f\"></i>\r\n                    </a>\r\n                </li>\r\n                <li class=\"px-3\">\r\n                    <a href=\"{{textCommingSoon.instagram}}\">\r\n                        <i class=\"fab fa-twitter\"></i>\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/commingsoon/commingsoon.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/commingsoon/commingsoon.component.ts ***!
  \*****************************************************************/
/*! exports provided: CommingsoonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommingsoonComponent", function() { return CommingsoonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");



var CommingsoonComponent = /** @class */ (function () {
    function CommingsoonComponent(mFconfig) {
        this.mFconfig = mFconfig;
        this.image = {};
        this.image = this.mFconfig.mFConfig.urlImage;
        this.textCommingSoon = this.mFconfig.mFConfig.contentCommingSoon;
    }
    CommingsoonComponent.prototype.ngOnInit = function () {
    };
    CommingsoonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-commingsoon',
            template: __webpack_require__(/*! ./commingsoon.component.html */ "./src/app/components/commingsoon/commingsoon.component.html"),
            styles: [__webpack_require__(/*! ./commingsoon.component.css */ "./src/app/components/commingsoon/commingsoon.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"]])
    ], CommingsoonComponent);
    return CommingsoonComponent;
}());



/***/ }),

/***/ "./src/app/components/contacto/contacto.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/contacto/contacto.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFjdG8vY29udGFjdG8uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/contacto/contacto.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/contacto/contacto.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container animated fadeIn\">\r\n  <div class=\"row\">\r\n    <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 my-5\" data-element=\"contacto\">\r\n      <h2 class=\"py-3\">Medios de contacto</h2>\r\n      <p>Para contactarnos lo puedes hacer de lunes a domingo en los horarios de 07:00 am - 06:00 pm por los siguiente medios:</p>\r\n      <h3><span class=\"badge\"><i class=\"fas fa-phone rotate-90\"></i></span> 0959638071</h3>\r\n      <h3><span class=\"badge\"><i class=\"fas fa-phone rotate-90\"></i></span> 0992506911</h3>\r\n      <h3><span class=\"badge\"><i class=\"fas fa-phone rotate-90\"></i></span> 0992114751</h3>\r\n      <hr>\r\n      <h3>\r\n        <a href=\"https://www.facebook.com/mobiliariofamarsa/\"> mobiliariofamarsa <span class=\"badge\"><i class=\"fab fa-facebook-f\"></i></span></a>\r\n      </h3>\r\n      <h3>\r\n        <a href=\"https://api.whatsapp.com/send?phone=593959638071&text=Hola%2C%20necesito%20una%20asesor%C3%ADa\"> +593 95 963 8071 <span class=\"badge\"><i class=\"fab fa-whatsapp\"></i></span></a>\r\n      </h3>\r\n      <h3>\r\n        <a href=\"https://twitter.com/MobiliarioFM\"> @MobiliarioFM<span class=\"badge\"><i class=\"fab fa-twitter\"></i></span></a>\r\n      </h3>\r\n    </div>\r\n    <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 my-5\">\r\n      <h1 class=\"text-center py-3\">Formulario de Contacto</h1>\r\n      <form #forma=\"ngForm\" class=\"py-3\">\r\n        <div class=\"form-group\">\r\n          <label for=\"names\">Nombres y Apellidos</label>\r\n          <input type=\"text\" name=\"names\" [(ngModel)]=\"contacto.names\"\r\n                 class=\"form-control\" id=\"names\" placeholder=\"Ingrese su nombre y apellidos\" required>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"email\">Correo</label>\r\n          <input type=\"email\" name=\"email\" [(ngModel)]=\"contacto.email\"\r\n                 class=\"form-control\" id=\"email\" placeholder=\"Ingrese su correo electrónico\" required>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <textarea name=\"asunto\" [(ngModel)]=\"contacto.asunto\" id=\"\" cols=\"10\" rows=\"5\" class=\"form-control\" required></textarea>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <button class=\"btn btn-block btn-primary m-auto w-50\" (click)=\"enviarContacto()\" [disabled]=\"!forma.valid\">Enviar</button>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>\r\n{{ response | json }}\r\n"

/***/ }),

/***/ "./src/app/components/contacto/contacto.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/contacto/contacto.component.ts ***!
  \***********************************************************/
/*! exports provided: ContactoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoComponent", function() { return ContactoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_email_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/email.service */ "./src/app/services/email.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);




var ContactoComponent = /** @class */ (function () {
    function ContactoComponent(emailService) {
        this.emailService = emailService;
        this.contacto = {
            names: '',
            email: '',
            asunto: ''
        };
        this.sendMail = emailService;
    }
    ContactoComponent.prototype.ngOnInit = function () {
    };
    ContactoComponent.prototype.enviarContacto = function () {
        var _this = this;
        this.sendMail.sendMail(this.contacto).subscribe(function (data) { return (_this.response = data); });
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
            title: 'Datos enviados con éxito!',
            text: 'Muy pronto nos pondremos en contacto con usted. Gracias por escribirnos',
            type: 'success',
            confirmButtonText: 'Aceptar'
        });
        // console.log(this.response);
        // this.clearInputs();
    };
    ContactoComponent.prototype.clearInputs = function () {
        this.contacto = {
            names: '',
            email: '',
            asunto: ''
        };
    };
    ContactoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contacto',
            template: __webpack_require__(/*! ./contacto.component.html */ "./src/app/components/contacto/contacto.component.html"),
            styles: [__webpack_require__(/*! ./contacto.component.css */ "./src/app/components/contacto/contacto.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_email_service__WEBPACK_IMPORTED_MODULE_2__["EmailService"]])
    ], ContactoComponent);
    return ContactoComponent;
}());



/***/ }),

/***/ "./src/app/components/empresa/empresa.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/empresa/empresa.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZW1wcmVzYS9lbXByZXNhLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/empresa/empresa.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/empresa/empresa.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container animated fadeIn\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12 col-xs-12 p-0\">\r\n      <h1 class=\"text-center py-5\">{{ config.title }}</h1>\r\n      <h2 class=\"text-center mb-3\">{{ config.subTitle }}</h2>\r\n    </div>\r\n    <div class=\"col-md-6 col-xs-12 pl-0\">\r\n      <h5>{{ config.content[0].title }}</h5>\r\n      <p>{{ config.content[0].parrafo }}</p>\r\n      <hr>\r\n      <h5>{{ config.content[1].title }}</h5>\r\n      <p>{{ config.content[1].parrafo }}</p>\r\n      <hr>\r\n      <h5>{{ config.content[2].title }}</h5>\r\n      <p>{{ config.content[2].parrafo }}</p>\r\n    </div>\r\n    <div class=\"col-md-6 col-xs-12 pl-0\">\r\n      <img [src]=\"config.img1\" class=\"w-100 animated fadeIn\" alt=\"\">\r\n    </div>\r\n    <br>\r\n    <hr>\r\n    <div class=\"col-md-12 col-xs-12\">\r\n      <h5 class=\"border-secondary border-top pb-3 pt-4 text-center\">{{ config.content2[0].title }}</h5>\r\n    </div>\r\n    <div class=\"col-md-6 col-xs-12\">\r\n      <p class=\"text-center\">{{ config.content2[1].value }}</p>\r\n      <p class=\"text-center\">{{ config.content2[3].value }}</p>\r\n      <p class=\"text-center\">{{ config.content2[5].value }}</p>\r\n    </div>\r\n    <div class=\"col-md-6 col-xs-12\">\r\n      <p class=\"text-center\">{{ config.content2[2].value }}</p>\r\n      <p class=\"text-center\">{{ config.content2[4].value }}</p>\r\n      <p class=\"text-center\">{{ config.content2[6].value }}</p>\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n      <h5 class=\"text-center\">{{ config.content3.title }}</h5>\r\n    </div>\r\n  </div>\r\n</div>\r\n<app-map [configMap]=\"config.content3.map\"></app-map>\r\n"

/***/ }),

/***/ "./src/app/components/empresa/empresa.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/empresa/empresa.component.ts ***!
  \*********************************************************/
/*! exports provided: EmpresaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpresaComponent", function() { return EmpresaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");



var EmpresaComponent = /** @class */ (function () {
    function EmpresaComponent(mFconfig) {
        this.mFconfig = mFconfig;
        this.config = {};
        this.config = this.mFconfig.empresaData;
    }
    EmpresaComponent.prototype.ngOnInit = function () {
    };
    EmpresaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-empresa',
            template: __webpack_require__(/*! ./empresa.component.html */ "./src/app/components/empresa/empresa.component.html"),
            styles: [__webpack_require__(/*! ./empresa.component.css */ "./src/app/components/empresa/empresa.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"]])
    ], EmpresaComponent);
    return EmpresaComponent;
}());



/***/ }),

/***/ "./src/app/components/galeria/galeria.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/galeria/galeria.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container animated fadeIn\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12 my-5\">\r\n      <h2 class=\"text-center\">\r\n        Galería\r\n      </h2>\r\n    </div>\r\n\r\n  </div>\r\n  <div class=\"row\" *ngIf=\"images\">\r\n    <div class=\"item_gallery col-12 col-sm-12 col-md-4 col-lg-3 animated fadeInUp clickIn\" *ngFor=\"let img of images\" [ngClass]=\"clsHover\"\r\n         (click)=\"showImg($event,img)\" [ngStyle]=\"{'background': 'url(' + (img)  + ') no-repeat','background-size':'cover','background-position':'center'}\"\r\n         style=\"width:30%; height:310px;\">\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div *ngIf=\"modalIn\" class=\"modal_image animated fadeIn\" (click)=\"hideModalIn($event)\">\r\n  <img [src]=\"srcModal\" alt=\"\" class=\"animated fadeInUp\">\r\n</div>\r\n{{ images }}\r\n"

/***/ }),

/***/ "./src/app/components/galeria/galeria.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/galeria/galeria.component.ts ***!
  \*********************************************************/
/*! exports provided: GaleriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaComponent", function() { return GaleriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");
/* harmony import */ var _ngx_gallery_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-gallery/core */ "./node_modules/@ngx-gallery/core/fesm5/ngx-gallery-core.js");
/* harmony import */ var _ngx_gallery_lightbox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-gallery/lightbox */ "./node_modules/@ngx-gallery/lightbox/fesm5/ngx-gallery-lightbox.js");
/* harmony import */ var _services_fireservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/fireservice.service */ "./src/app/services/fireservice.service.ts");






var GaleriaComponent = /** @class */ (function () {
    function GaleriaComponent(config, gallery, lightbox, fireService) {
        this.config = config;
        this.gallery = gallery;
        this.lightbox = lightbox;
        this.fireService = fireService;
        this.mFconfig = {};
        // @ts-ignore
        this.imageData = this.config.galeriaData.content;
        this.mFconfig = this.config.galeriaData;
        this.modalIn = false;
        this.fireServ = fireService;
    }
    GaleriaComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.fireServ.getCollectionFire('gallery', 'gallery')
                                .then(function (res) { return res.imgs; })];
                    case 1:
                        _a.images = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GaleriaComponent.prototype.showImg = function (e, src) {
        e.preventDefault();
        this.modalIn = true;
        this.srcModal = src;
    };
    GaleriaComponent.prototype.hideModalIn = function (e) {
        e.preventDefault();
        this.modalIn = false;
    };
    GaleriaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-galeria',
            template: __webpack_require__(/*! ./galeria.component.html */ "./src/app/components/galeria/galeria.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"],
            _ngx_gallery_core__WEBPACK_IMPORTED_MODULE_3__["Gallery"],
            _ngx_gallery_lightbox__WEBPACK_IMPORTED_MODULE_4__["Lightbox"],
            _services_fireservice_service__WEBPACK_IMPORTED_MODULE_5__["FireserviceService"]])
    ], GaleriaComponent);
    return GaleriaComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\r\n  <ngb-carousel *ngIf=\"config\" interval=\"7000\" >\r\n    <ng-template ngbSlide *ngFor=\"let img of config.slider;let i = index \">\r\n      <img [src]=\"img.src\" [alt]=\"img.titulo\" [title]=\"img.titulo\" class=\"w-100 animated fadeInLeft\">\r\n      <div class=\"carousel-caption animated slideInUp\">\r\n        <h3>{{ img.titulo }}</h3>\r\n        <p>{{ img.parrafo1 }} <br> {{ img.parrafo2 }} </p>\r\n        <p *ngIf=\"img.link != ''\">\r\n          <a [href]=\"img.link\" class=\"btn btn-danger\">Ver más</a>\r\n        </p>\r\n      </div>\r\n    </ng-template>\r\n  </ngb-carousel>\r\n\r\n  <div class=\"container pt-3\">\r\n    <h4 class=\"bg-danger mt-5 py-3 text-center text-light\">{{ config.subTitlehome }}</h4>\r\n    <div class=\"row\" id=\"content-products\">\r\n      <div class=\"col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 py-3\" *ngFor=\"let miniatures of config.miniatures\">\r\n        <div class=\"card w-100\">\r\n          <a [href]=\"miniatures.enlace\">\r\n            <div class=\"cortina\">\r\n              <p>{{ miniatures.title }}</p>\r\n            </div>\r\n            <img class=\"card-img-top\" [src]=\"miniatures.src\" [alt]=\"miniatures.title\">\r\n          </a>\r\n          <div class=\"card-body\">\r\n            <p class=\"card-text\">{{ miniatures.description }}</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <h4 class=\"bg-warning mt-5 py-3 text-center text-capitalize\">{{ config.contentDescrition.title }}</h4>\r\n    <div class=\"row mb-4\" id=\"content-descrip\">\r\n      <div class=\"col-md-12 col-xs-12\">\r\n        <p>{{ config.contentDescrition.texto1 }}</p>\r\n      </div>\r\n      <div class=\"col-md-12 col-xs-12\">\r\n        <p>{{ config.contentDescrition.texto2 }}</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <app-map [configMap]=\"config.map\"></app-map>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");




var HomeComponent = /** @class */ (function () {
    function HomeComponent(MfConfig, ngConfig) {
        this.MfConfig = MfConfig;
        this.ngConfig = ngConfig;
        this.config = '';
        this.config = MfConfig.getConfig();
        ngConfig.interval = 10000;
        ngConfig.wrap = false;
        ngConfig.keyboard = false;
        ngConfig.pauseOnHover = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbCarouselConfig"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbCarouselConfig"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/home/map/map.component.html":
/*!********************************************************!*\
  !*** ./src/app/components/home/map/map.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"maps-angular\" class=\"mt-5\">\r\n<!--  <agm-map [latitude]=\"configMap.lat\" [longitude]=\"configMap.lng\" [zoom]=\"configMap.zoom\">\r\n    <agm-marker [latitude]=\"configMap.lat\" [longitude]=\"configMap.lng\"></agm-marker>\r\n  </agm-map>-->\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n      <h6 class=\"text-center\">Almacén #1</h6>\r\n      <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.8139330836934!2d-76.90643998524332!3d0.08496846435672993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e2823eefba5b231%3A0x431e37f0f4c23041!2sMobiliarios+Famarsa!5e0!3m2!1ses-419!2sco!4v1558234280189!5m2!1ses-419!2sco\"\r\n              width=\"100%\" height=\"400\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <h6 class=\"text-center\">Almacén #2</h6>\r\n      <iframe src=\"https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d249.36337311180213!2d-76.90365007077753!3d0.08461228179928205!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sco!4v1666983973249!5m2!1ses-419!2sco\"\r\n              width=\"100%\" height=\"400\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/home/map/map.component.ts":
/*!******************************************************!*\
  !*** ./src/app/components/home/map/map.component.ts ***!
  \******************************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MapComponent = /** @class */ (function () {
    function MapComponent() {
        this.configMap = {};
    }
    MapComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MapComponent.prototype, "configMap", void 0);
    MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-map',
            template: __webpack_require__(/*! ./map.component.html */ "./src/app/components/home/map/map.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/components/portafolios/portafolios.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/portafolios/portafolios.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item-img-portfolio{\r\n  border: solid 1px #eee;\r\n  border-radius: 5px;\r\n}\r\n.btn-preguntar-por{\r\n  background: #dc3545;\r\n  bottom: 0;\r\n  color: #fff;\r\n  text-align: center;\r\n  font-weight: bold;\r\n  font-style: italic;\r\n  border-radius: 0 15px 0 0;\r\n  padding-bottom: 10px;\r\n  padding-top: 10px;\r\n  /*opacity: 0;*/\r\n  left: -80%;\r\n  transition: all 500ms cubic-bezier(0.420, 0.000, 0.580, 1.000); /* ease-in-out */\r\n  transition-timing-function: cubic-bezier(0.420, 0.000, 0.580, 1.000); /* ease-in-out */\r\n}\r\n.btn-preguntar-por span{\r\n  font-size: 0.5rem;\r\n  position: absolute;\r\n  right: 15px;\r\n  top: 10px;\r\n}\r\n.btn-preguntar-por:hover{\r\n  opacity: 1;\r\n  left: 0%;\r\n}\r\n.item_gallery{\r\n  -webkit-clip-path: inset(0% 0% 0% 5%);\r\n          clip-path: inset(0% 0% 0% 5%);\r\n}\r\n.item_gallery:hover{\r\n  box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.55);\r\n}\r\n.btn-show-img{\r\n  height: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wb3J0YWZvbGlvcy9wb3J0YWZvbGlvcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtDQUNwQjtBQUNEO0VBQ0Usb0JBQW9CO0VBQ3BCLFVBQVU7RUFDVixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFdBQVc7RUFDWCwrREFBK0QsQ0FBQyxpQkFBaUI7RUFDakYscUVBQXFFLENBQUMsaUJBQWlCO0NBQ3hGO0FBQ0Q7RUFDRSxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixVQUFVO0NBQ1g7QUFDRDtFQUNFLFdBQVc7RUFDWCxTQUFTO0NBQ1Y7QUFDRDtFQUNFLHNDQUE4QjtVQUE5Qiw4QkFBOEI7Q0FDL0I7QUFDRDtFQUdFLDZDQUE2QztDQUM5QztBQUNEO0VBQ0UsYUFBYTtDQUNkIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9wb3J0YWZvbGlvcy9wb3J0YWZvbGlvcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLml0ZW0taW1nLXBvcnRmb2xpb3tcclxuICBib3JkZXI6IHNvbGlkIDFweCAjZWVlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG4uYnRuLXByZWd1bnRhci1wb3J7XHJcbiAgYmFja2dyb3VuZDogI2RjMzU0NTtcclxuICBib3R0b206IDA7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICBib3JkZXItcmFkaXVzOiAwIDE1cHggMCAwO1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIC8qb3BhY2l0eTogMDsqL1xyXG4gIGxlZnQ6IC04MCU7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDUwMG1zIGN1YmljLWJlemllcigwLjQyMCwgMC4wMDAsIDAuNTgwLCAxLjAwMCk7IC8qIGVhc2UtaW4tb3V0ICovXHJcbiAgdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLjQyMCwgMC4wMDAsIDAuNTgwLCAxLjAwMCk7IC8qIGVhc2UtaW4tb3V0ICovXHJcbn1cclxuLmJ0bi1wcmVndW50YXItcG9yIHNwYW57XHJcbiAgZm9udC1zaXplOiAwLjVyZW07XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAxNXB4O1xyXG4gIHRvcDogMTBweDtcclxufVxyXG4uYnRuLXByZWd1bnRhci1wb3I6aG92ZXJ7XHJcbiAgb3BhY2l0eTogMTtcclxuICBsZWZ0OiAwJTtcclxufVxyXG4uaXRlbV9nYWxsZXJ5e1xyXG4gIGNsaXAtcGF0aDogaW5zZXQoMCUgMCUgMCUgNSUpO1xyXG59XHJcbi5pdGVtX2dhbGxlcnk6aG92ZXJ7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMHB4IDVweCAwcHggcmdiYSgwLDAsMCwwLjU1KTtcclxuICAtbW96LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKDAsMCwwLDAuNTUpO1xyXG4gIGJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKDAsMCwwLDAuNTUpO1xyXG59XHJcbi5idG4tc2hvdy1pbWd7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/portafolios/portafolios.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/portafolios/portafolios.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container animated fadeIn\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12 my-5\">\r\n      <h2 class=\"text-center\">\r\n        Catálogo{{ currentUrl }}\r\n      </h2>\r\n    </div>\r\n\r\n  </div>\r\n  <div class=\"row pb-5\" *ngIf=\"images\">\r\n    <div class=\"item_gallery col-12 col-sm-12 col-md-4 col-lg-3 animated fadeInUp clickIn pb-4\" *ngFor=\"let img of images\">\r\n      <div class=\"item-img-portfolio position-relative\" [ngStyle]=\"{'background': 'url(' + (img)  + ') no-repeat','background-size':'100%','background-position':'center'}\" style=\"height:310px;\">\r\n        <div class=\"btn-show-img w-100 position-absolute\" (click)=\"showImg($event,img)\"></div>\r\n        <div class=\"btn-preguntar-por w-100 position-absolute\" (click)=\"callWpGo(img)\" >\r\n          <p class=\"text-white mb-0\">Cotizar</p>\r\n          <span><i class=\"fab fa-3x fa-whatsapp\"></i></span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div *ngIf=\"modalIn\" class=\"modal_image animated fadeIn\" (click)=\"hideModalIn($event)\">\r\n  <img [src]=\"srcModal\" alt=\"\" class=\"animated fadeIn w-50\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/portafolios/portafolios.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/portafolios/portafolios.component.ts ***!
  \*****************************************************************/
/*! exports provided: PortafoliosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortafoliosComponent", function() { return PortafoliosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_gallery_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-gallery/core */ "./node_modules/@ngx-gallery/core/fesm5/ngx-gallery-core.js");
/* harmony import */ var _ngx_gallery_lightbox__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-gallery/lightbox */ "./node_modules/@ngx-gallery/lightbox/fesm5/ngx-gallery-lightbox.js");
/* harmony import */ var _services_fireservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/fireservice.service */ "./src/app/services/fireservice.service.ts");







var PortafoliosComponent = /** @class */ (function () {
    function PortafoliosComponent(MfConfig, router, fireService, gallery, lightbox) {
        this.MfConfig = MfConfig;
        this.router = router;
        this.fireService = fireService;
        this.gallery = gallery;
        this.lightbox = lightbox;
        this.paused = false;
        this.pauseOnHover = true;
        this.URL = 'https://api.whatsapp.com/send?phone=+593959638071&text=Quiero%20m%C3%A1s%20informaci%C3%B3n%20sobre%20este%20articulo%20';
        this.fireServ = fireService;
        this.modalIn = false;
        this.currentUrl = this.router.url;
    }
    PortafoliosComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.fireServ.getCollectionFire('gallery', 'portfolio')
                                .then(function (res) {
                                switch (_this.router.url) {
                                    case '/sillas':
                                        return res.sillas;
                                    case '/archivadores':
                                        return res.archivadores;
                                    case '/estaciones':
                                        return res.estaciones;
                                    case '/cafeinternet':
                                        return res.cafeinternet;
                                    case '/closets':
                                        return res.closets;
                                    case '/counters':
                                        return res.counters;
                                    case '/cocinas':
                                        return res.cocinas;
                                    case '/divisiones':
                                        return res.divisiones;
                                    case '/escritorios':
                                        return res.escritorios;
                                    case '/estanterias':
                                        return res.estanterias;
                                    case '/gavetas':
                                        return res.gavetas;
                                    case '/mesas':
                                        return res.mesas;
                                    default:
                                        return null;
                                }
                            })];
                    case 1:
                        _a.images = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PortafoliosComponent.prototype.showImg = function (e, src) {
        e.preventDefault();
        this.modalIn = true;
        this.srcModal = src;
    };
    PortafoliosComponent.prototype.hideModalIn = function (e) {
        e.preventDefault();
        this.modalIn = false;
    };
    PortafoliosComponent.prototype.callWpArticulo = function (url) {
        return this.URL + url;
    };
    PortafoliosComponent.prototype.callWpGo = function (url) {
        var myUrl = this.callWpArticulo(url);
        window.open(myUrl, '_blank');
    };
    PortafoliosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-portafolios',
            template: __webpack_require__(/*! ./portafolios.component.html */ "./src/app/components/portafolios/portafolios.component.html"),
            styles: [__webpack_require__(/*! ./portafolios.component.css */ "./src/app/components/portafolios/portafolios.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_fireservice_service__WEBPACK_IMPORTED_MODULE_6__["FireserviceService"],
            _ngx_gallery_core__WEBPACK_IMPORTED_MODULE_4__["Gallery"],
            _ngx_gallery_lightbox__WEBPACK_IMPORTED_MODULE_5__["Lightbox"]])
    ], PortafoliosComponent);
    return PortafoliosComponent;
}());



/***/ }),

/***/ "./src/app/components/publicaciones/publicaciones.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/components/publicaciones/publicaciones.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHVibGljYWNpb25lcy9wdWJsaWNhY2lvbmVzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/publicaciones/publicaciones.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/publicaciones/publicaciones.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12 my-5\">\r\n      <h2 class=\"text-center\">\r\n        Publicaciones / Instagram\r\n      </h2>\r\n    </div>\r\n\r\n  </div>\r\n  <div id=\"instafeed-container\" class=\"row pb-5\" >\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/publicaciones/publicaciones.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/publicaciones/publicaciones.component.ts ***!
  \*********************************************************************/
/*! exports provided: PublicacionesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicacionesComponent", function() { return PublicacionesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_instagram_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/instagram.service */ "./src/app/services/instagram.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var PublicacionesComponent = /** @class */ (function () {
    function PublicacionesComponent(iGService) {
        this.iGService = iGService;
        this.posts = null;
    }
    PublicacionesComponent.prototype.ngOnInit = function () {
        // @ts-ignore
        var userFeed = new Instafeed({
            get: 'user',
            target: 'instafeed-container',
            resolution: 'standard_resolution',
            // tslint:disable-next-line:max-line-length
            template: "<div class='item_gallery col-12 col-sm-12 col-md-4 col-lg-3 animated fadeInUp clickIn' style=\"background: url('{{image}}') no-repeat; background-size: cover; height: 250px; background-position: center;\"></div>",
            accessToken: _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].Instafeed.accessToken,
            limit: 24,
        });
        userFeed.run();
    };
    PublicacionesComponent.prototype.mostrar = function () {
        console.log('hola!!!');
    };
    PublicacionesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-publicaciones',
            template: __webpack_require__(/*! ./publicaciones.component.html */ "./src/app/components/publicaciones/publicaciones.component.html"),
            styles: [__webpack_require__(/*! ./publicaciones.component.css */ "./src/app/components/publicaciones/publicaciones.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_instagram_service__WEBPACK_IMPORTED_MODULE_2__["InstagramService"]])
    ], PublicacionesComponent);
    return PublicacionesComponent;
}());



/***/ }),

/***/ "./src/app/components/shared/chats/chats.component.css":
/*!*************************************************************!*\
  !*** ./src/app/components/shared/chats/chats.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2NoYXRzL2NoYXRzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/shared/chats/chats.component.html":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/chats/chats.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"chats\">\r\n  <ul>\r\n    <li>\r\n      <a href=\"https://api.whatsapp.com/send?phone=593959638071&text=Hola%2C%20necesito%20una%20asesor%C3%ADa\" target=\"_blank\">\r\n        <span>Escríbenos !! </span><i class=\"fab fa-3x fa-whatsapp\"></i>\r\n      </a>\r\n    </li>\r\n  </ul>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/shared/chats/chats.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/shared/chats/chats.component.ts ***!
  \************************************************************/
/*! exports provided: ChatsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsComponent", function() { return ChatsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ChatsComponent = /** @class */ (function () {
    function ChatsComponent() {
    }
    ChatsComponent.prototype.ngOnInit = function () {
    };
    ChatsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chats',
            template: __webpack_require__(/*! ./chats.component.html */ "./src/app/components/shared/chats/chats.component.html"),
            styles: [__webpack_require__(/*! ./chats.component.css */ "./src/app/components/shared/chats/chats.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ChatsComponent);
    return ChatsComponent;
}());



/***/ }),

/***/ "./src/app/components/shared/footer/footer.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/shared/footer/footer.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"app-footer\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 mt-4\">\r\n        <legend class=\"text-light border-bottom\">{{ mFconfig.footer.col1.title }}</legend>\r\n        <ul class=\"list-inline\">\r\n          <li *ngFor=\"let item of mFconfig.footer.col1.items\">\r\n            <p class=\"text-light m-0\">\r\n              {{ item.title }} : {{ item.value }}\r\n            </p>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"col-md-6 mt-4\">\r\n        <legend class=\"text-light border-bottom\">{{ mFconfig.footer.col2.title }}</legend>\r\n        <ul class=\"list-inline\">\r\n          <li *ngFor=\"let item of mFconfig.footer.col2.items\">\r\n            <p class=\"text-light m-0\">\r\n              {{ item.title }} : {{ item.value }}\r\n            </p>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div id=\"footer-bajo\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <p class=\"text-light font-weight-light text-light p-2 m-1\">\r\n            <span>{{ mFconfig.footerBajo.col1.copy }} {{current_Year}}</span>\r\n            <a [href]=\"mFconfig.footerBajo.col1.page\" class=\"text-light\"><span> {{ mFconfig.footerBajo.col1.page }} </span></a>\r\n            <span><span>{{ mFconfig.footerBajo.col1.right }}</span></span>\r\n          </p>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <p class=\"text-light font-weight-light text-light p-2 m-1 text-right\">\r\n            <span><a [href]=\"mFconfig.footerBajo.col2.link\" class=\"text-light\">{{ mFconfig.footerBajo.col2.createdBy }}</a></span>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/shared/footer/footer.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/footer/footer.component.ts ***!
  \**************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");



var FooterComponent = /** @class */ (function () {
    function FooterComponent(config) {
        this.config = config;
        this.mFconfig = {};
        this.mFconfig = this.config.getConfig();
        this.current_Year = new Date().getFullYear();
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/shared/footer/footer.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/shared/header/header.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/shared/header/header.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"preloader === false\" id=\"preloader\" class=\"d-flex justify-content-center animated\">\r\n  <img src=\"assets/img/balls_loading.gif\" alt=\"preloader\">\r\n</div>\r\n\r\n<!--menu movil-->\r\n<nav id=\"menuMovil\" [ngClass]=\"showMenuMovil ? 'hola bounceInLeft' : 'zoomOutLeft'\"  class=\"animated\">\r\n  <a [routerLink]=\"'home'\">\r\n    <img [src]=\"mFConfig.urlImage\" class=\"w-75 mb-5\" alt=\"{{ mFConfig.nombre }}\" title=\"{{ mFConfig.nombre }}\">\r\n  </a>\r\n  <ul>\r\n    <li *ngFor=\"let item of mFConfig.menu\" class=\"nav-item\" routerLinkActive=\"active\">\r\n\r\n      <div *ngIf=\"item.sub === null\">\r\n        <a class=\"nav-link text-light\" [routerLink]=\"item.link\">{{ item.name }}</a>\r\n      </div>\r\n      <div *ngIf=\"item.sub != null\">\r\n        <a class=\"nav-link dropdown-toggle text-light\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n          {{ item.name }}\r\n        </a>\r\n        <div class=\"dropdown-menu bg-danger\">\r\n          <a *ngFor=\"let itmSub of item.sub\" class=\"dropdown-item text-light\" [routerLink]=\"itmSub.link\">\r\n            {{ itmSub.name }}\r\n          </a>\r\n        </div>\r\n      </div>\r\n\r\n    </li>\r\n  </ul>\r\n</nav>\r\n<!--menu movil-->\r\n\r\n\r\n<!-- Image and text -->\r\n<div class=\"pb-4 header-bg\">\r\n  <div class=\"container\">\r\n    <div class=\"d-flex bd-highlight\">\r\n      <div class=\"pt-4 flex-fill bd-highlight w-25\">\r\n        <a [routerLink]=\"'home'\">\r\n          <img [src]=\"mFConfig.urlImage\" class=\"w-75\" alt=\"{{ mFConfig.nombre }}\" title=\"{{ mFConfig.nombre }}\">\r\n        </a>\r\n      </div>\r\n      <div class=\"p-2 flex-fill bd-highlight\">\r\n        <div class=\"float-md-right icons-header mt-5\">\r\n          <a href=\"https://twitter.com/MobiliarioFM\" target=\"_blank\">\r\n            <i class=\"fab fa-twitter\"></i>\r\n          </a>\r\n          <a href=\"https://www.facebook.com/mobiliariofamarsa/\" target=\"_blank\">\r\n            <i class=\"fab fa-facebook-f\"></i>\r\n          </a>\r\n          <a [routerLink]=\"'contacto'\">\r\n            <i class=\"fas fa-envelope\"></i>\r\n          </a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<nav class=\"navbar-light bg-danger sticky-top\">\r\n  <div class=\"container\">\r\n      <ul class=\"nav justify-content-center\" id=\"v_desktop\">\r\n        <li *ngFor=\"let item of mFConfig.menu\" class=\"nav-item\" routerLinkActive=\"active\">\r\n\r\n          <div *ngIf=\"item.sub === null\">\r\n            <a class=\"nav-link text-light\" [routerLink]=\"item.link\">{{ item.name }}</a>\r\n          </div>\r\n          <div *ngIf=\"item.sub != null\">\r\n            <a class=\"nav-link dropdown-toggle text-light\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n              {{ item.name }}\r\n            </a>\r\n            <div class=\"dropdown-menu bg-danger\">\r\n              <a *ngFor=\"let itmSub of item.sub\" class=\"dropdown-item text-light\" [routerLink]=\"itmSub.link\">\r\n                {{ itmSub.name }}\r\n              </a>\r\n            </div>\r\n          </div>\r\n\r\n        </li>\r\n      </ul>\r\n    <div id=\"v_movil\">\r\n      <span (click)=\"showMenu($event)\" class=\"clickIn\"><i class=\"fas fa-bars fa-2x p-2\"></i></span>\r\n    </div>\r\n    </div>\r\n</nav>\r\n<app-chats></app-chats>\r\n"

/***/ }),

/***/ "./src/app/components/shared/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/shared/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/mfconfig.service */ "./src/app/services/mfconfig.service.ts");
/* harmony import */ var _services_fireservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/fireservice.service */ "./src/app/services/fireservice.service.ts");




var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(MfConfig, fireService) {
        var _this = this;
        this.MfConfig = MfConfig;
        this.fireService = fireService;
        this.mFConfig = '';
        this.preloader = false;
        this.showMenuMovil = false;
        this.fireServ = fireService;
        // this.mFConfig = MfConfig.getConfig();
        console.log(this.mFConfig);
        setTimeout(function () { _this.preloader = true; }, 3000);
    }
    HeaderComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.fireServ.getCollectionFire('configuracion', 'sitio')
                                .then(function (res) {
                                _this.mFConfig = res;
                                console.log(res);
                            })];
                    case 1:
                        _a.images = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HeaderComponent.prototype.showMenu = function (e) {
        e.preventDefault();
        this.showMenuMovil = this.showMenuMovil === false;
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/shared/header/header.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mfconfig_service__WEBPACK_IMPORTED_MODULE_2__["MfconfigService"], _services_fireservice_service__WEBPACK_IMPORTED_MODULE_3__["FireserviceService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/services/email.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/email.service.ts ***!
  \*******************************************/
/*! exports provided: EmailService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailService", function() { return EmailService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




// import {  }
var EmailService = /** @class */ (function () {
    function EmailService(httpClient) {
        this.httpClient = httpClient;
        this.urlTarget = 'https://mobiliariosfamarsa.com/api';
    }
    EmailService.prototype.sendMail = function (request) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json').set('cache-control', 'no-cache');
        return this.httpClient.post(this.urlTarget, request, { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            console.log(res);
        }));
    };
    EmailService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], EmailService);
    return EmailService;
}());



/***/ }),

/***/ "./src/app/services/fireservice.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/fireservice.service.ts ***!
  \*************************************************/
/*! exports provided: FireserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FireserviceService", function() { return FireserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.esm.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




var FireserviceService = /** @class */ (function () {
    function FireserviceService() {
        this.fireService = firebase__WEBPACK_IMPORTED_MODULE_2__["default"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].firebaseConfig);
    }
    FireserviceService.prototype.getCollectionFire = function (collection, document) {
        var db = this.fireService.firestore();
        return db.collection(collection).doc(document)
            .get().then(function (doc) {
            return doc.data();
        }).catch(function (err) {
            return null;
        });
    };
    FireserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FireserviceService);
    return FireserviceService;
}());



/***/ }),

/***/ "./src/app/services/instagram.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/instagram.service.ts ***!
  \***********************************************/
/*! exports provided: InstagramService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstagramService", function() { return InstagramService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var InstagramService = /** @class */ (function () {
    function InstagramService(httpClient) {
        this.httpClient = httpClient;
        this.feed = null;
        // private urlTarget = 'https://www.instagram.com/graphql/query/';
        this.urlTarget = 'http://localhost:3000/';
    }
    InstagramService.prototype.getFeed = function () {
        console.log('lanzando esto');
        // @ts-ignore
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]({
            // @ts-ignore
            query_id: '17888483320059182',
            variables: JSON.stringify({ id: '7133246744', first: 20, after: null })
        });
        // return this.httpClient.get(this.urlTarget, { params: params});
        return this.httpClient.get(this.urlTarget);
    };
    InstagramService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], InstagramService);
    return InstagramService;
}());



/***/ }),

/***/ "./src/app/services/mfconfig.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/mfconfig.service.ts ***!
  \**********************************************/
/*! exports provided: MfconfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MfconfigService", function() { return MfconfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MfconfigService = /** @class */ (function () {
    function MfconfigService() {
        this.mFConfig = {
            nombre: 'Mobiliarios Famarsa',
            urlImage: '../assets/img/logo_Mobiliarios-famarsa.png',
            menu: [
                { 'name': 'Inicio', 'link': '/home', 'sub': null },
                { 'name': 'Empresa', 'link': '/empresa', 'sub': null },
                { 'name': 'Catálogo', 'link': null, 'sub': [
                        { 'name': 'Estaciones', 'link': '/estaciones', 'sub': null },
                        { 'name': 'Estanterías', 'link': '/estanterias', 'sub': null },
                        { 'name': 'Sillas', 'link': '/sillas', 'sub': null },
                        { 'name': 'Mesas', 'link': '/mesas', 'sub': null },
                        { 'name': 'Counters', 'link': '/counters', 'sub': null },
                        { 'name': 'Archivadores', 'link': '/archivadores', 'sub': null },
                        { 'name': 'Mesas', 'link': '/mesas', 'sub': null },
                        { 'name': 'Escritorios', 'link': '/escritorios', 'sub': null },
                        // {'name': 'Credenzas', 'link': '/credenzas', 'sub': null},
                        { 'name': 'Closets', 'link': '/closets', 'sub': null },
                        { 'name': 'Cocinas', 'link': '/cocinas', 'sub': null },
                        { 'name': 'Gavetas', 'link': '/gavetas', 'sub': null },
                        { 'name': 'Divisiones', 'link': '/divisiones', 'sub': null },
                        { 'name': 'Café Internet', 'link': '/cafeinternet', 'sub': null }
                    ] },
                // {'name': 'Publicaciones', 'link': '/publicaciones', 'sub': null},
                { 'name': 'Contacto', 'link': '/contacto', 'sub': null },
            ],
            slider: [
                {
                    'titulo': 'Bienvenidos a \n Mobiliarios Famarsa',
                    'parrafo1': 'Donde siempre queremos que te sientas cómodo.',
                    'parrafo2': 'Haz de tu oficina un lugar agradable',
                    'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/slider6.jpg?alt=media&token=b70d05b5-6c6c-49b1-96cb-20b9a6547361',
                    'link': '',
                    'target': ''
                },
                {
                    'titulo': 'En Mobiliarios Famarsa',
                    'parrafo1': 'Trabajamos para darte lo mejor.',
                    'parrafo2': 'Siempre calidad al mejor precio !!',
                    'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/slider8.jpg?alt=media&token=c6b7c827-b500-403e-86fb-6f52c81291d6',
                    'link': '',
                    'target': ''
                },
                {
                    'titulo': 'Estaciones de trabajo',
                    'parrafo1': 'La más completa colección y variedad de estaciones de trabajo que desees.',
                    'parrafo2': '',
                    'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/slider1.jpg?alt=media&token=0e6345f8-3753-4b05-9314-4e4c9a05c191',
                    'link': '#',
                    'target': ''
                },
                {
                    'titulo': 'Sillas de espera',
                    'parrafo1': 'Tenemos grán variedad de sillas de espera y de consultorio.',
                    'parrafo2': 'Anímate a ver nuestro catálogo',
                    'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/slider7.jpg?alt=media&token=33178393-5716-4e45-bba4-d59bc83af2ac',
                    'link': '#',
                    'target': ''
                }
            ],
            subTitlehome: 'Muebles de Oficina y para Negocios',
            miniatures: [
                { 'title': 'Estaciones de Trabajo', 'description': 'Pensadas y creadas a tu manera, para una mejor comodidad', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F04_min-300x250.png?alt=media&token=15bb3ea1-e5e5-492e-8abd-d16205a4980b', 'enlace': 'estaciones' },
                { 'title': 'Sillas giratorias y de espera', 'description': 'Diseño y comodidad de ultima generación', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F04_min2-300x250.png?alt=media&token=9302c139-e059-42e8-a177-f07ca04ce9d1', 'enlace': '#' },
                { 'title': 'Counters', 'description': 'Con todo el espacio necesario para llevar tu orden', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F01_min-300x250.png?alt=media&token=95754258-1d7b-4f5a-98b4-d31ed3d11adb', 'enlace': '#' },
                { 'title': 'Archivadores', 'description': 'La mejor forma de llevar registro de tus documentos', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F02_minarc-300x250.png?alt=media&token=0973c7dd-31e5-4a4f-b04d-a1fe634bfaa9', 'enlace': '#' },
                { 'title': 'Mesas', 'description': 'Amplias y con el mejor diseño', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F04_minmensas-300x250.png?alt=media&token=7d33c831-b5e7-4ab7-b30f-6d4893afd1c6', 'enlace': '#' },
                { 'title': 'Escritorios', 'description': 'Diseño y distribución de espacios', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F11_min-1-300x250.png?alt=media&token=e5dc9709-c59e-4087-b821-50cca9f6ca7c', 'enlace': '#' },
                { 'title': 'Estanterías', 'description': 'Espacios de soportes para cualquier elemento', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F04_minestanterias-300x250.png?alt=media&token=5d696486-ced0-4dc0-aca7-87762a50d3ee', 'enlace': '#' },
                { 'title': 'Credemzas', 'description': 'Mejor distribución de espacios para tus documentos', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F03_min-300x250.png?alt=media&token=8a8dd566-0ada-4c09-8cb2-ba2129d1f37a', 'enlace': '#' },
                { 'title': 'Closets y armarios', 'description': 'Amplios y con excelente distribución de espacios', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F01_mincloset-300x250.png?alt=media&token=6531616b-d33d-428b-b9a2-c622fd70bef1', 'enlace': '#' },
                { 'title': 'Cocinas integrales', 'description': 'Diseño y tamaño a tu manera con excelentes materiales', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F03_mincocinas-300x250.png?alt=media&token=ef8425bb-eecb-406c-932c-8efc2d1b68dd', 'enlace': '#' },
                { 'title': 'Divisiones y cubículos', 'description': 'Distribución de espacios para elementos de computo y documentos', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F09_min-300x250.png?alt=media&token=e9f82d77-05e6-4b70-a39f-91b4b5ba3f1e', 'enlace': '#' },
                { 'title': 'Muebles para café internet', 'description': 'La mejor forma de organizar el espacio de tu negocio con excelente diseño y material', 'src': 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/miniatures%2F07_min-300x250.png?alt=media&token=ec544bb6-0c91-48fa-8c56-e0e66459acfb', 'enlace': '#' }
            ],
            contentDescrition: {
                title: 'Mobiliarios famarsa',
                texto1: 'Todo lo relacionado con Muebles de oficina, Somos fabricantes de los mejores artículos con la mejor calidad, estamos ubicados en Lago agrio, Sucumbios, Ecuador.' +
                    ' Producción, suministro e instalación de sistemas modulares para oficina. A través de desarrollos propios damos respuesta a diversos requerimientos espaciales ajustados a los presupuestos de nuestros clientes, involucrando conceptos tecnológicos universales apoyados en un equipo multidisciplinario de desarrollo de producto y arquitectura. Contamos con la infraestructura necesaria para suministrar y si se requiere instalar proyectos en toda la región amazónica de ecuador.',
                texto2: 'Durante toda nuestra historia nos hemos categorizado como una empresa que provee productos de altísima calidad. Es por esto que uno de nuestros servicios es la comercialización de partes y componentes de mobiliario y sillas a empresas, proveedores y clientes. Teniendo en cuenta nuestra tecnología podemos producir grandes cantidades de partes plásticas de diferentes colores para suplir su proyecto del mejor producto con las características indicadas'
            },
            contentCommingSoon: {
                title: 'Espera esta sección muy pronto',
                textParrafo: 'Actualmente estamos trabajano para habilitar estos espacios y poder ofrecerte todo los productos de Mobiliarios Famarsa y que puedas' +
                    ' tener una mejor experiencia en nuestro sitio web. Es por eso que estamos tomanonos un tiempo para ti. Mientras tanto te invitamos' +
                    'a visitar nuestros perfiles en la redes sociales: ',
                facebook: 'https://www.facebook.com/mobiliariofamarsa/',
                instagram: 'https://www.instagram.com/mobiliarios_famarsa/'
            },
            map: {
                lat: 0.085028,
                lng: -76.904310,
                zoom: 15
            },
            footer: {
                col1: {
                    title: 'Dirección',
                    items: [
                        { 'title': 'Dir', 'value': 'Via Quito Km 2 1/2' },
                        { 'title': 'Barrio', 'value': 'Oro negro Diag Roman Hermanos' },
                        { 'title': 'Lago agrio', 'value': 'Sucumbios - Ecuador' }
                    ]
                },
                col2: {
                    title: 'Contacto',
                    items: [
                        { 'title': 'Ing', 'value': 'Fabriciano Martínez Sánchez' },
                        { 'title': 'Tels', 'value': '0959638071 - 0992506911' },
                        { 'title': 'Sec', 'value': 'General: Odalinda Muentes' },
                        { 'title': 'Tel', 'value': '0992114751' }
                    ]
                }
            },
            footerBajo: {
                col1: {
                    copy: 'Copyright © 2014 -',
                    page: 'mobiliariosfamarsa.com',
                    right: 'All rights reserved.'
                },
                col2: {
                    createdBy: 'Created By Eric Js Mtz',
                    link: '#'
                }
            }
        };
        this.empresaData = {
            title: 'Mobiliarios Famarsa',
            subTitle: 'Nuestra Empresa',
            img1: 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/gallery%2FWhatsApp%20Image%202019-05-20%20at%205.20.42%20PM.jpeg?alt=media&token=43b8f96e-2e85-4cde-836e-b9e9b4c8a2af',
            img2: 'https://firebasestorage.googleapis.com/v0/b/mobiliariosfamarsaangular.appspot.com/o/gallery%2FWhatsApp%20Image%202019-05-20%20at%205.20.42%20PM.jpeg?alt=media&token=43b8f96e-2e85-4cde-836e-b9e9b4c8a2af',
            content: [
                { 'title': '¿Quienes Somos?', 'parrafo': 'Somo una empresa fabricantes de muebles para toda clase de sitios como empresas, hogares, negocios y establecimientos públicos, Somos Mobiliarios Famarsa.' },
                { 'title': 'Misión', 'parrafo': 'Mobiliarios famarsa Somos una empresa nueva con innovación en la mercadotecnia que busca el más alto grado de satisfacción del cliente al ofrecer excelentes servicio y soluciones para mobiliarios de oficinas y colectividad, basándonos en criterios funcionales, de diseño, calidad y oportunos tiempos de entrega. Siempre tomando en cuenta las tendencias del mercado y adaptándonos a las necesidades de nuestros cliente' },
                { 'title': 'Visión', 'parrafo': 'Mobiliarios famarsa Nos destacamos en el mercado del Oriente ecuatoriano por cumplir con los más exigentes requerimientos de calidad al elaborar mobiliarios de oficina y colectividad con tecnología de mayor precisión e innovación, lo cual nos permitirá expandir nuestros mercado a paises vecinos.' },
            ],
            content2: [
                { 'title': 'Valores', 'value': 'Valores' },
                { 'title': 'Calidad', 'value': 'Calidad' },
                { 'title': 'Honestidad', 'value': 'Honestidad' },
                { 'title': 'Compromiso', 'value': 'Compromiso' },
                { 'title': 'Creatividad e Innovación', 'value': 'Creatividad e Innovación' },
                { 'title': 'Eficiencia', 'value': 'Eficiencia' },
                { 'title': 'Capital Humano', 'value': 'Capital Humano' }
            ],
            content3: {
                title: 'Ubicación',
                map: {
                    lat: 0.085028,
                    lng: -76.904310,
                    zoom: 15
                }
            }
        };
        this.galeriaData = {
            title: 'Galería de Imágenes',
            content: [
                { 'title': 'img1', 'src': 'assets/img/gallery/gallery_01.jpg' },
                { 'title': 'img2', 'src': 'assets/img/gallery/gallery_02.jpg' },
                { 'title': 'img3', 'src': 'assets/img/gallery/gallery_03.jpg' },
                { 'title': 'img4', 'src': 'assets/img/gallery/gallery_04.jpg' },
                { 'title': 'img5', 'src': 'assets/img/gallery/gallery_05.jpg' },
                { 'title': 'img6', 'src': 'assets/img/gallery/gallery_06.jpg' },
                { 'title': 'img7', 'src': 'assets/img/gallery/gallery_07.jpg' },
                { 'title': 'img8', 'src': 'assets/img/gallery/gallery_08.jpg' },
                { 'title': 'img9', 'src': 'assets/img/gallery/gallery_09.jpeg' },
                { 'title': 'img10', 'src': 'assets/img/gallery/gallery_10.jpeg' },
                { 'title': 'img11', 'src': 'assets/img/gallery/gallery_11.jpeg' },
                { 'title': 'img12', 'src': 'assets/img/gallery/gallery_12.jpeg' },
                { 'title': 'img13', 'src': 'assets/img/gallery/gallery_13.jpeg' }
            ]
        };
        this.estaciones = [62, 83, 466, 965, 982, 1043, 738].map(function (n) { return "https://picsum.photos/id/" + n + "/900/500"; });
    }
    MfconfigService.prototype.getConfig = function () {
        return this.mFConfig;
    };
    MfconfigService.prototype.getEmpresaData = function () {
        return this.empresaData;
    };
    MfconfigService.prototype.getPortfolio = function (type) {
        var response;
        switch (type) {
            case '/estaciones':
                response = this.estaciones;
                break;
            case '/sillas':
                response = this.estaciones;
                break;
            case '/counters':
                response = this.estaciones;
                break;
            case '/archivadores':
                response = this.estaciones;
                break;
            default:
                break;
        }
        return response;
    };
    MfconfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MfconfigService);
    return MfconfigService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: true,
    firebaseConfig: {
        apiKey: 'AIzaSyC9FSAK14MfdpXKrjFUYLP7Mhg_GAbBJ-Q',
        authDomain: 'mobiliariosfamarsaangular.firebaseapp.com',
        databaseURL: 'https://mobiliariosfamarsaangular.firebaseio.com',
        projectId: 'mobiliariosfamarsaangular',
        storageBucket: 'mobiliariosfamarsaangular.appspot.com',
        messagingSenderId: '966574565821',
        appId: '1:966574565821:web:26ba527da607844f8bcad4'
    },
    Instafeed: {
        // tslint:disable-next-line:max-line-length
        accessToken: 'IGQVJVVkpRNjByQmNKNFpVSUREMVlDRDBtZADBwOXoydC1RVWVhbUlDTWJNdVZAUZAkhvSThaTGgzSGJUUW4wYUNxdFhoSF9BNmZA6THJ6cGo1TTNjbWRhdE1JLXc2M0ZABeGZAOd2R1SkNBdmlrREg5dDdLSAZDZD'
        // accessToken: 'IGQVJXNHluOWl5bTR4eWE2STVxLTNHaWhqVVdHWlNBMU9oUXRGeGVZAQUFtdHIxVFZA4bi1odmJIeVlqSXZAmZA2tGX3AwR0twRk9pVnBueEJzdEJXcThrNnAwQW9pYVZA2cUtfSW5WT1I3d1VsUDhUelVjdAZDZD'
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\user\Documents\proyectos\angular\mobiliariosfamarsa\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map