export interface Contacto {
    names: string;
    email: string|number;
    asunto: string;
}
